#!/usr/bin/env python

import sys

test_data="""
NODE_8_length_333034_cov_6.554101	8	333034	6.55	30.34	0	Clostridium
NODE_9_length_320753_cov_5.970798	9	320753	5.97	29.78	10	Clostridium
NODE_15_length_216830_cov_6.122371	15	216830	6.12	29.62	30	Clostridium
NODE_16_length_213889_cov_5.802899	16	213889	5.80	29.78	42	Clostridium
NODE_21_length_193840_cov_10.505282	21	193840	10.51	30.99	0	Clostridium
NODE_24_length_183349_cov_12.380667	24	183349	12.38	29.38	0	Clostridium
NODE_25_length_163077_cov_5.338519	25	163077	5.34	28.44	10	Clostridium
"""

keep_names = {}
for line in open(sys.argv[1],'r'):
	line = line.strip()
	if line[0]==">":
		prot_name = line[1:].split(' ')[0]
		keep_names[prot_name] = True

ko_data = """
NODE_8_length_333034_cov_6.554101_1	K02069	ABC.X2.P; putative ABC transport system permease protein	216		
NODE_8_length_333034_cov_6.554101_2			10	K11623	5
NODE_8_length_333034_cov_6.554101_3	K07695	devR; two-component system, NarL family, response regulator DevR	38		
NODE_8_length_333034_cov_6.554101_4			8	K17363	2
"""

for line in open(sys.argv[2],'r'):
	lsplits = line.split("\t")
	prot_name = lsplits[0]
	#cov = float(lsplits[3])
	#if cov >= 8.3:
	#	sys.stdout.write(line)
	if prot_name in keep_names:
		sys.stdout.write(line)
