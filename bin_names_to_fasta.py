#!/usr/bin/env python

'''
./bin_names_to_fasta.py scaffolds.fasta bin1_2_stats.tab bin3_stats.tab bin4_stats.tab bin5_6_stats.tab bin7_stats.tab bin8_stats.tab bin9_stats.tab
'''


import sys
from Bio import SeqIO

if len(sys.argv)<2:
	sys.exit(__doc__)
else:
	scaf_file = sys.argv[1]
	sys.stderr.write("# Reading fasta scaffolds from {}\n".format( scaf_file ) )
	scaf_dict = SeqIO.to_dict(SeqIO.parse( scaf_file , "fasta") )
	sys.stderr.write("# Contains {} scaffolds\n".format( len(scaf_dict) ) )

	used_scaffolds = {} # key is scaffold, value is True
	for binfile in sys.argv[2:]:
		write_count = 0
		binned_fasta_name = binfile.replace(".tab",".fasta")
		with open(binned_fasta_name,'w') as bf:
			for line in open(binfile,'r'):
				lsplits = line.split("\t")
				scaf_name = lsplits[0]
				if scaf_name in used_scaffolds:
					sys.stderr.write("##SKIPPING {}\n".format( scaf_name ) )
					continue
				write_count += 1
				bf.write( scaf_dict[scaf_name].format("fasta") )
		sys.stderr.write("# {} contains {} scaffolds\n".format( binfile, write_count ) )
