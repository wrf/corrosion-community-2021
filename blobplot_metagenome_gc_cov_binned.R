
unbin_data = read.table("~/git/corrosion-community-2021/climate_lake1_scaffolds.stats.w_genus.tab", header=TRUE, sep="\t")
#magnituderange = range(log(unbin_data[["length"]],base=10))
magnituderange = c(2.5, 5) # use set range
pchsize = log(unbin_data[["length"]],base=10)-magnituderange[1]

pdf(file="~/git/corrosion-community-2021/climate_lake1_scaffolds.bins_vs_unbinned.logscale.pdf", width=8, height=6)
par(mar=c(4.5,4.5,1,1.5))
plot(unbin_data[["coverage"]], unbin_data[["GC"]], type='p', log='x', xlim=c(2,800), ylim=c(20,70), xlab="Mean contig coverage", ylab="GC%", pch=16, frame.plot=FALSE, col="#00000019", cex.axis=1.5, cex=pchsize, main="", cex.lab=1.4) # "Climate lake metagenomic community"
# get bin size counts
bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin01_high_coverage_clostridium.stats.tab",sep="\t")
length(bin_data[["V3"]])
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]]) / sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#8497b0aa" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin02_bacteroides.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#3ca6b288" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin03_desulfovibrio.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#00c04baa" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin04_mid_coverage_clostridium.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#5b687cbb" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin05_low_cov_clostridium_smear.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#6e9eedbb" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin06_low_cov_clostridium.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#2f3642cc" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin07_low_cov_clostridium.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#203764cc" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin08_methanosaeta.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#6f0500cc" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin09_methanobacterium.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#8d3935aa" )

bin_data = read.table("~/git/corrosion-community-2021/binned_scaffold_stats/bin10_low_cov_clostridium.stats.tab",sep="\t")
sum(bin_data[["V3"]])
sum(bin_data[["V3"]]*bin_data[["V4"]])/sum(bin_data[["V3"]])
points(bin_data[["V4"]], bin_data[["V5"]], cex=c(log(bin_data[["V3"]],base=10)-magnituderange[1]), pch=16, col="#3d5b5fcc" )

text(500,50, "(1) Clostridium", font=3 ) # Clostridium main 500x
text(110,45, "(2) Bacteroides", font=3 ) # Bacteroides
text(15, 65, "(3) Desulfovibrio", pos=4, font=3 ) # Desulfovibrio
text(90, 37, "(4) Clostridium", pos=2, font=3 ) # Clostridium 100x
text(180,32, "(5) Clostridium", pos=4, font=3 ) # Clostridium low cov smear
text(50, 26, "(6) Clostridium", font=3 ) # Clostridium 50x
text(15, 25, "(7) Clostridium", font=3 ) # Clostridium 14x
text(6, 54, "(8) Methanosaeta", pos=4, font=3 ) # Methanosaeta
text(8, 40, "(9) Methanobacterium", pos=4, font=3 ) # Methanothermobacter
text(5, 23, "(10) Clostridium", font=3 ) # Clostridium 7x

dev.off()

