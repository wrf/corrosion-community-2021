#!/usr/bin/env python

import sys

keep_dict = {}
for line in open(sys.argv[1],'r'):
	line = line.strip()
	keep_dict[line] = True

for line in open(sys.argv[2],'r'):
	lsplits = line.split('\t')
	contig = lsplits[0]
	if contig not in keep_dict:
		sys.stdout.write(line)


