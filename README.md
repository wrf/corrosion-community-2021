# Corrosion community 2021 #

Supplemental metagenomic data for paper:

Palacios, PA., WR. Francis, AE. Rotaru (2021) [Win-loss interactions on Fe0 between methanogens and acetogens from a climate lake](https://doi.org/10.3389/fmicb.2021.638282). Frontiers in Microbiology 12:638282.

Raw sequencing data (1x metagenome library, 6x amplicon libraries) can be found at [NCBI BioProject PRJNA713576](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA713576).

### [Data downloads](https://bitbucket.org/wrf/corrosion-community-2021/downloads/) ###

* [all_scaffolds.fasta.gz](https://bitbucket.org/wrf/corrosion-community-2021/downloads/all_scaffolds.fasta.gz), all 84742 scaffolds directly from metaSPAdes
* [binned_scaffolds.zip](https://bitbucket.org/wrf/corrosion-community-2021/downloads/binned_scaffolds.zip), zip of scaffolds of the 10 bins (10 files in the folder)
* [binned_proteins_w_KEGG_tables.zip](https://bitbucket.org/wrf/corrosion-community-2021/downloads/binned_proteins_w_KEGG_tables.zip), zip of proteins of all 10 bins and the KEGG annotation tables (20 files in folder)
* [scaffolds.prodigal.prots.fa.gz](https://bitbucket.org/wrf/corrosion-community-2021/downloads/scaffolds.prodigal.prots.fa.gz), FASTA format of all proteins from all scaffolds
* [scaffolds.prodigal.gff.gz](https://bitbucket.org/wrf/corrosion-community-2021/downloads/scaffolds.prodigal.gff.gz), GFF format file of all protein annotations
* [climate_lake1_scaffolds.stats.w_genus.tab.gz](https://bitbucket.org/wrf/corrosion-community-2021/downloads/climate_lake1_scaffolds.stats.w_genus.tab.gz), tab-delimited text file of GC-coverage stats of all scaffolds, with top genus-level annotation from [MG-RAST](https://www.mg-rast.org/linkin.cgi?project=mgp97220) (using RefSeq)

![climate_lake1_scaffolds.bins_vs_unbinned.logscale.pdf.png](https://bitbucket.org/wrf/corrosion-community-2021/raw/25bf651fcf0b0ecc7b8b4d8625b0722f9135fbf0/images/climate_lake1_scaffolds.bins_vs_unbinned.logscale.pdf.png)

### Pre-assembly data QC ###
Checking kmer frequency using [jellyfish](https://github.com/gmarcais/Jellyfish) to plot with [lavaLampPlot](https://github.com/wrf/lavaLampPlot). This produced Figure 3S.

`gzip -dc SDU_*.gz | ~/jellyfish/jellyfish-linux count -m 31 -s 1G -C -o SDU_lib.counts -t 4 /dev/fd/0`

`~/jellyfish/jellyfish-linux dump SDU_lib.counts > SDU_lib.dumps`

`~/jellyfish/jellyfish-linux dump fastq.counts | ~/git/fastqdumps2histo.py -k 31 -u 1000 -j fastq.dumps - > SDU_lib.gc_cov.histo.csv`

`Rscript ~/git/lavaLampPlot/jellyfish_gc_coverage_blob_plot_v2.R SDU_lib.gc_cov.histo.csv SDU_lib.gc_cov.histo.pdf`

### Metagenome assembly ###
Assembling metagenome with [metaSPAdes](https://github.com/ablab/spades)

`~/SPAdes-3.14.1-Linux/bin/metaspades.py -1 SDU_1.fastq.gz -2 SDU_2.fastq.gz -t 6 -m 40 -o climate_lake1`

This gives 84742 sequences: 

* Total bases: 109.2 Mb
* Average length: 1288 bp
* Longest seq: 489770 bp
* N50: 6784 bp

Coverage and GC stats were calculated using [spadescontigstocovgc.py](https://bitbucket.org/wrf/sequences/src/master/spadescontigstocovgc.py) to plot with [lavaLampPlot](https://github.com/wrf/lavaLampPlot)

`spadescontigstocovgc.py scaffolds.fasta > climate_lake1_scaffolds.stats.tab`

`Rscript ~/git/lavaLampPlot/contig_gc_coverage.R climate_lake1_scaffolds.stats.tab`

This made the rough version of Figure 6, which was refined for the paper with the script `blobplot_metagenome_gc_cov_binned.R`.

| bin | mg_rast_top_annotation | num_contigs | total_Mb | bin_N50_kb | num_sp | mean_coverage_adj | genes | KEGG_annotated | pct_KEGG | by_hit_filtering |
| :-- | --- | ---: | ---: | ---: | ---: | ---: | ---: | ---: | ---: | --- |
| 1 | Clostridium | 365 | 9.39 | 61.3 | 2 | 541.0689 | 8776 | 4894 | 55.7657247037 | n |
| 2 | Bacteroides or Parabacteroides | 67 | 4.17 | 97.9 | 1 | 125.6138 | 3396 | 1618 | 47.6442873969 | n |
| 3 | Desulfovibrio | 145 | 3.1 | 63.2 | 1 | 11.47156 | 2917 | 1565 | 53.651011313 | n |
| 4 | Clostridium | 30 | 2.66 | 278.9 | 1 | 92.88796 | 2584 | 1411 | 54.6052631579 | n |
| 5 | Clostridium | 603 | 10.97 | 43.5 | 2 | 115.6814 | 10600 | 5020 | 47.358490566 | n |
| 6 | Clostridium | 105 | 2.96 | 62 | 1 | 45.78777 | 2909 | 1492 | 51.2891027845 | n |
| 7 | Clostridium | 265 | 3.9 | 58 | 1 | 11.55116 | 3915 | 1771 | 45.2362707535 | n |
| 8 | Methanosaeta | 115 | 2.26 | 32.9 | 1 | 4.871729 | 2328 | 1155 | 49.6134020619 | y |
| 9 | Methanothermobacter | 40 | 1.35 | 63.9 | 1 | 5.899643 | 1364 | 658 | 48.2404692082 | y |
| 10 | Clostridium | 460 | 5.12 | 60.5 | 1 | 5.844339 | 5197 | 2381 | 45.8148932076 | n |

### Protein annotation ###
Proteins were annotated off the scaffolds using [prodigal](https://github.com/hyattpd/Prodigal)

`~/Prodigal_v2.6.3/prodigal.linux -f gff -a scaffolds.prodigal.prots.fa -i scaffolds.fasta -o scaffolds.prodigal.gff -p meta`

This gives 163458 proteins:

* Total amino acids: 31M AAs
* Average prot length: 193 AAs
* Shortest prot: 20 AAs
* Longest prot: 5096 AAs

